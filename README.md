# [kosmetolog.org](https://kosmetolog.org) source codes

<br/>

### Run kosmetolog.org on localhost

    # vi /etc/systemd/system/kosmetolog.org.service

Insert code from kosmetolog.org.service

    # systemctl enable kosmetolog.org.service
    # systemctl start kosmetolog.org.service
    # systemctl status kosmetolog.org.service

http://localhost:4036
